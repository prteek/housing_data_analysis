# Housing data analysis and modelling

#### Objective: To build a model that predicts guest room usage

[Master Analysis](https://nbviewer.jupyter.org/github/prteek/housing_data_analysis/blob/master/housing_analysis_master.ipynb) <br>
[Mean value model analysis](https://nbviewer.jupyter.org/github/prteek/housing_data_analysis/blob/master/mean_value_model_analysis.ipynb) <br>  
